#!/bin/bash
# For Custom Dynamic DNS behavior for Dreamhost
# DNS host and bash-capable router

# init
# change to YOUR API Key
# (https://help.dreamhost.com/hc/en-us/articles/4407354972692)
DNSAPIKey="1A1A1A1A1A1A1A1A"
DNSName="video.perivid.xyz"
DNSRecordType="A"

# get current external WAN IPv4 address
existingIP=$(curl -s -L "https://icanhazip.com/")

# if we don't have a ./value.dat file, start with current value
if [ ! -f "./value.dat" ] ; then
  storedValue=$existingIP
  # otherwise read the value from the file
else
  storedValue=`cat ./value.dat`
fi

# If there's no change, don't update anything
# If there has been a change, remove old DNS entry and
# add new one.
# NOTE: Dreamhost doesn't allow updating existing records, so this
# is the only way.
if [ $storedValue != $existingIP ] ; then
  printf "IP CHANGED since last check\n" | logger
  printf "Command to remove DNS entry:\n" | logger
  curl -s -L "https://api.dreamhost.com/?key=$DNSAPIKey&cmd=dns-remove_record&record=$DNSName&type=$DNSRecordType&value=$storedValue\n\n" | logger
  printf "Command to add new DNS entry:\n" | logger
  curl -s -L "https://api.dreamhost.com/?key=$DNSAPIKey&cmd=dns-add_record&record=$DNSName&type=$DNSRecordType&value=$existingIP\n\n" | loggeer
else
  printf "No IP Change" | logger
fi

# and save it for next time
echo "${existingIP}" > ./value.dat
